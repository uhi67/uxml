Uxml
====

Version 1.2 -- 2021-05-06

An XML processing library. Extends DOMDocument and DOMElement standard classes.

Uses a default built-in or specified external formatter to format various data types to string values.

Installation
------------

Add to your `composer.json`:

    {
      "require": {
        "uhi67/uxml": "*"
      },
    }

Usage examples
--------------
```
$doc = new UXMLDoc();
$doc->documentElement->addNode('new node', ['attr'=>'value']);
```

### Array representation of nodeList (see addNodeList)

content:
- string
- array of nodes

node:

- array(nodename, array of attributes, content)

Example: 
```
[['alma', ['ize'=>'finom'], 'Gála'], ['körte', ['ize'=>'kásás'], [['fajta', ['name' => 'Boscobak']], ['text', null, 'szöveg']]
```
### Array representation of subnode (see addSubnode, createNode)
content:
- string (text node)
- array with numeric key => text (text nodes 'text')
- array element with _nodename key => content (subnodes with name 'nodename') !!! Warning: multiple subnodes with same name is not possible
- array element name => scalar value (attribute with name 'name' and value)

Example:
``` 
['_alma' => ['ize'=>'finom', 'Gála'], '_körte' => ['ize'=>'kásás', '_fajta' => ['name' => 'Boscobak']], '_text' => 'szöveg']
```
### Array representation of subnode (see createSubnode)
content:
- string
- array of subcontent

subcontent:

- numeric => text
- subnodename => array of multiple contents. Creates multiple subnodes with same name.
- attributename => value

Example: 
```
['alma' => [['ize'=>'finom', 'Gála']], 'körte' => [['ize'=>'kásás', 'fajta' => [['name' => 'Boscobak']]], 'text' => [['szöveg']]]
```

Change log
==========

Version 1.2 -- 2021-05-06
-------------------------
- UXMLElement::relativePath() added

Version 1.1 - 2020-12-16
------------------------
- createNode() callbacks argument order changed!

Version 1.0 - 2020-11-02
------------------------
- first public release

2019-08-23
----------
- library moved into git repository

### Removed deprecated methods

- UXMLElement::queryTree() --> use Models and Queryies in UXApp
- UXMLElement::query() --> use Query::createSql($sql, [$id], $this->db)->buildNodes($node_parent, $nodename) in UXApp
- UXMLElement::addFields() --> use Query::buildAttributes() in UXApp
- UXMLElement::setQueryAttributes() --> use Query::buildAttributes() in UXApp
- UXMLDoc::adXXX() --> use similar methods of UXMLElement 

### Changed

- UXMLDoc::__construct(): no default xslpath and locale, formatter can be specified

### New

- FormatterInterface
- Formatter class
