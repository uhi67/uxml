<?php /** @noinspection PhpIllegalPsrClassPathInspection */
/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

/** @noinspection PhpUnused */

namespace uhi67\uxml;

use Closure;
use DOMAttr;
use DOMDocument;
use DOMDocumentFragment;
use DOMElement;
use DOMException;
use DOMNode;
use DOMNodeList;
use DOMXPath;
use Exception;

/**
 * # Class UXMLElement
 *
 * Extended DOM functions
 *
 * ### Array representation of nodeList (see {@see addNodeList()})
 *
 * content:
 * - string
 * - array of nodes
 *
 * node:
 * - array(nodename, array of attributes, content)
 *
 * Example: [['alma', ['ize'=>'finom'], 'Gála'], ['körte', ['ize'=>'kásás'], [['fajta', ['name' => 'Boscobak']], ['text', null, 'szöveg']]
 *
 * ### Array representation of subnode (see {@see addSubnode(), @see createNode()})
 *
 * content:
 * 	- string (text node)
 * 	- array with numeric key => text (text nodes 'text')
 * 	- array element with _nodename key => content (subnodes with name 'nodename') !!! Warning: multiple subnodes with same name is not possible
 * 	- array element name => scalar value (attribute with name 'name' and value)
 *
 * Example: ['_alma' => ['ize'=>'finom', 'Gála'], '_körte' => ['ize'=>'kásás', '_fajta' => ['name' => 'Boscobak']], '_text' => 'szöveg']
 *
 * ### Array representation of subnode (see {@see createSubnode()})
 *
 * content:
 * - string
 * - array of subcontent
 *
 * subcontent:
 * - numeric => text
 * - subnodename => array of multiple contents. Creates multiple subnodes with same name.
 * - attributename => value
 *
 * Example: ['alma' => [['ize'=>'finom', 'Gála']], 'körte' => [['ize'=>'kásás', 'fajta' => [['name' => 'Boscobak']]], 'text' => [['szöveg']]]
 *
 * See also: {@see UXMLDoc}
 *
 * @property UXMLDoc $ownerDocument
 * @property-read UXMLElement $firstChild
 * @property-read UXMLElement $parentNode
 * @property-read string $xml -- outer xml of the node
 * @method UXMLElement appendChild() appendChild(DOMNode $node1)
 * @method UXMLNodeList getElementsByTagname() getElementsByTagname(string $name)
 */
class UXMLElement extends DOMElement {
    /**
     * Retrieves the value of an array element or object property with the given key or property name.
     * If the key does not exist in the array or object, the default value will be returned.
     *
     * The key may be specified as array like `['x', 'y', 'z']`.
     *
     * Examples
     *
     * ```php
     * // working with array
     * $username = ArrayUtils::getValue($_POST, 'username');
     * // working with object
     * $username = ArrayUtils::getValue($user, 'username');
     * // working with anonymous function
     * $fullName = ArrayUtils::getValue($user, function ($user, $defaultValue) {
     *     return $user->firstName . ' ' . $user->lastName;
     * });
     * // using an array of keys to retrieve the value
     * $value = ArrayUtils::getValue($versions, ['1.1', 'date']);
     * ```
     *
     * @param array|object $array array or object to extract value from
     * @param string|Closure|array $key key name of the array element, an array of keys or property name of the object,
     * or an anonymous function returning the value. The anonymous function signature should be:
     * `function($array, $defaultValue)`.
     * @param mixed $default the default value to be returned if the specified array key does not exist. Not used when
     * getting value from an object.
     *
     * @return mixed the value of the element if found, default value otherwise
     */
    public static function getValue($array, $key, $default = null) {
        if(is_null($array)) return $default;
        if ($key instanceof Closure) {
            return $key($array, $default);
        }

        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }

        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array)) ) {
            return $array[$key];
        }

        if (is_object($array)) {
            try {
                // this is expected to fail if the property does not exist, or __get() is not implemented
                /** @noinspection PhpVariableVariableInspection */
                return $array->$key;
            }
            catch(Exception $e) {
                return $default;
            }
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }

    /**
     * Generates a valid XML name-id based on given string
     *
     * Replaces invalid characters to valid ones. Replaces accented letters to ASCII letters.
     *
     * - Element names must start with a letter or underscore
     * - Element names can contain letters, digits, underscores, and the specified enabled characters
     * - Element names cannot contain spaces
     *
     * @param string $str
     * @param string $def -- replace invalid characters to, default _
     * @param string $ena -- more enabled characters, e.g. '-' (specify - last, escape ] chars.)
     * @param int $maxlen -- maximum length or 0 if no limit. Default is 64.
     *
     * @return string -- the correct output, or empty if input was empty or null
     */
    public static function toNameID($str, $def='_', $ena='.-', $maxlen=64) {
        if($str=='' || $str===null) return $str;
        #if(strtolower(substr($str,0,3))=='xml') $str = '_'.$str; // xml prefix is valid!
        if($maxlen>0 && strlen($str)>$maxlen) $str = substr($str, 0, $maxlen);
        if(($p = strpos($ena, '-')) < strlen($ena)-1) $ena = substr($ena,0,$p).substr($ena,$p+1).'-';
        if(preg_match("~^[A-Za-z_][\w_$ena]*$~", $str)) return $str;
        if($def===null) $def='_';
        if($ena===null) $ena='';
        if(!preg_match("~^[A-Za-z_]~", $str)) $str = '_'.$str;
        $str = str_replace(
            preg_split('//u', 'áéíóöőúüűÁÉÍÓÖŐÚÜŰ', -1, PREG_SPLIT_NO_EMPTY),
            str_split('aeiooouuuAEIOOOUUU'),
            $str
        );
        if(strlen($ena)>0) $ena = mb_ereg_replace('(.)', '\\\\1', $ena);
        return mb_ereg_replace('[^A-Za-z0-9'.$def.$ena.']', $def, $str);
    }

    /**
     * Returns substring before delimiter
     *
     * @param string $s -- string
     * @param string $d -- delimiter
     * @param bool $full -- returns full string if pattern not found
     * @return string -- substring to delimiter or empty string if not found
     */
    public static function substring_before($s, $d, $full=false) {
        $p = strpos($s, $d);
        if($full && $p===false) return $s;
        return substr($s, 0, $p);
    }


    /**
     * Inserts elements of given xmldoc under the node
     *
     * @param DOMDocument $xmldoc -- The document to insert elements of
     *
     * @return UXMLElement -- the original node
     * @deprecated, use {@see addXmlContent()}
     */
    public function addDoc($xmldoc) {
        $node2 = $xmldoc->documentElement;
        $nodex = $node2->firstChild;
        while($nodex) {
            $this->appendChild($this->ownerDocument->importNode($nodex, true));
            $nodex = $nodex->nextSibling;
        }
        return $this;
    }

    /**
     * Adds new node with attributes and content to parent node.
     *
     * ### Attributes is universal content data (see addContent())
     *
     * - Literal data creates text content in the node.
     * - string => literal array elements: attribute values. Null attribute values will not create the attribute.
     * - string => array elements: creates subnodes (array of universal content data items)
     * - numeric index -> scalar: text content (DOM: subtree)
     * - numeric index -> array: Subnodes [nodename, contentdata]
     *
     * Special attribute:
     * - _insert_before => DOMElement: insert the new node before this reference node instead of append.
     *
     * ### Content is universal content data again :-) This behavior is compatible with the former definition.
     *
     * ### Namespace
     * - may be null or empty string - no namespace will be added
     * - array - the namespace with prefix as index will be used
     *     - if prefix not found in indices and namespace has element at 1-index, it will be used
     *       - or if no prefix, and namespace has element at 0-index, it will be used
     *     - otherwise no namespace will be added
     *
     * @param string|array $name -- nodename of created node or array(name, namespace) @see addNodes
     * @param string|array $attributes -- associative array
     * @param string|array $content -- text content or contents for multiple text nodes
     * @param string|array $namespace namespace of node or namespace array to associate with current prefix
     *
     * @return UXMLElement -- the inserted node
     * @throws
     * @see addContent()
     */
    public function addNode($name, $attributes=null, $content=null, $namespace=null) {
        /** @var string $ns1 -- namespace of the node */
        $ns1 = null;
        if(is_array($name)) {
            if(!array_key_exists(1, $name)) throw new DomException('Invalid node name (array must have 2 elements)');
            $ns1 = $name[1];
            $name  = $name[0];
        }
        $prefix = static::substring_before($name, ':');
        if(is_string($namespace)) {
            if($ns1) {
                $namespace = [$namespace];
            }
            else {
                $ns1 = $namespace;
                if($prefix) $namespace = [$prefix=>$ns1];
                else $namespace = null;
            }
        }
        if(is_array($namespace)) {
            if($ns1) {
                if($prefix) $namespace[$prefix] = $ns1;
                else {
                    if(isset($namespace[0])) $namespace[1] = $namespace[0];
                    $namespace[0] = $ns1;
                }
            }
            else {
                if ($prefix && isset($namespace[$prefix])) $ns1 = $namespace[$prefix];
                else if ($prefix && isset($namespace[1])) $ns1 = $namespace[1];
                else if (!$prefix && isset($namespace[0])) $ns1 = $namespace[0];
                else $ns1 = null;
            }
        }

        $insert_reference = is_array($attributes) ? static::fetchValue($attributes, '_insert_before') : null;
        static::assertClass($insert_reference, DOMElement::class);

        $node1 = $ns1 ?
            $this->ownerDocument->createElementNS($ns1, static::toNameID($name, '_', '_.-:')) :
            $this->ownerDocument->createElement(static::toNameID($name, '_', '_.-'));
        $node1 = $insert_reference ? $this->insertBefore($node1, $insert_reference) : $this->appendChild($node1);

        $node1->addContent($attributes, $namespace);
        $node1->addContent($content, $namespace);
        return $node1;
    }

    /**
     * ## Adds an attribute to the node
     *
     * Handles special value types as:
     * - Array: creates multiple text nodes instead of attributes
     *
     * Skips attribute names containing invalid characters
     *
     * If value is an array, subnodes will be created with content as value element
     *
     * Namespace will be added to prefixed attribute names.
     *
     * @param string $name -- optionally prefixed name
     * @param array|string $value -- array value to generate subnodes
     * @param mixed $namespace -- single namespace or prefix-indexed array of namespaces
     *
     * @return DOMAttr|UXMLElement|null -- the first inserted subnode, the inserted attribute node, or null if invalid name
     * @throws
     */
    public function addAttribute($name, $value, $namespace=null) {
        if($value===null) return null; // skip null attributes
        if(!is_array($value) && !is_string($value)) $value = $this->ownerDocument->toString($value);
        $prefix = static::substring_before($name, ':');

        // Prepare namespace
        if($prefix!='') {
            if(is_array($namespace) && isset($namespace[$prefix]))
                $this->addNamespaceDeclaration($prefix, $namespace[$prefix]);
            if(is_string($namespace) && $namespace!='')
                $this->addNamespaceDeclaration($prefix, $namespace);
        }

        if(is_array($value)) {
            // Insert subnode
            $n1 = null;
            foreach($value as $k=>$v) if($k!=='count') {
                $n = $this->addTextNode($name, $v);
                if(!$n1) $n1 = $n;
            }
            return $n1;
        }
        else {
            $name = static::toNameID($name, '-', '_:.-');
            if(preg_match('/^[_a-zA-Z][:a-zA-Z0-9_.-]*$/', $name)) {
                $this->setAttribute($name, $this->ownerDocument->toString($value));
                return $this->getAttributeNode($name);
            }
        }
        return null;
    }

    /** @noinspection PhpUnused */

    /**
     * ## Adds attributes to the node
     * Handles special value types by the formatter
     *
     * @see addAttribute
     *
     * @param mixed[] $values -- name-value pairs. The values will be formatted
     *
     * @return UXMLElement -- self
     */
    public function addAttributes($values) {
        foreach($values as $name=>$value) $this->addAttribute($name, $value);
        return $this;
    }


    /**
     * Creates a list of similar nodes from each item of values array.
     * Indices of values will be converted into 'index' attributes
     *
     * @param string|array $name -- name of created node or [name, namespace] (see {@see addNode})
     * @param array $values -- array of universal content data ({@see addContent()})
     * @param string|array $namespace -- (!!!options removed)
     *
     * @return UXMLElement -- the parent node itself (!!!changed)
     *
     * TODO: check all usages, interface changed.
     * @throws DOMException
     */
    public function addNodes($name, $values, $namespace=null) {
        foreach($values as $index => $contentData) {
            $this->addNode($name, ['index'=>$index], $contentData, $namespace);
        }
        return $this;
    }

    /**
     * Creates a subnode with given content.
     *
     * values is an array of the content or a text node:
     *        - numeric key => text node
     *        - _nodename => subnode content
     *        - attribute name => value
     *
     * @param string $tagname
     * @param string|array $values -- content, attribute and subnode values
     *
     * @return UXMLElement
     * @throws DOMException
     * @throws DomException
     *
     * @deprecated use {@see addNode}($tagname, $values)
     * @codeCoverageIgnore
     */
    public function addSubNode($tagname, $values) {
        if(is_array($values)) {
            $content = [];
            $subnodes = [];
            foreach($values as $k=>$v) {
                if(is_int($k)) {
                    $content[] = $v;
                    unset($values[$k]);
                }
                if(substr($k, 0, 1)=='_') {
                    $subnodes[substr($k,1)] = $v;
                    unset($values[$k]);
                }
            }
            $node = $this->addNode($tagname, $values, $content);
            foreach($subnodes as $sk=>$sv) /** @noinspection PhpDeprecationInspection */
                $node->addSubNode($sk, $sv);
        }
        else $node = $this->addNode($tagname, null, $values);

        return $node;
    }

    /** @noinspection PhpUnused */

    /**
     * ## Adds new node(s) based on node-path expression
     *
     * - Creates all parent nodes specified in the path if not exist
     * - Null attribute values will not create attributes.
     * - Array attribute values will generate series of subnodes with array element values as text value
     * - The generated node may not conform to the path specification if the attributes differ from path condition.
     *
     * @param string $name -- node-path (xpath expression) 'nodename[cond]/nodename[cond]/...nodename[cond]' format
     * @param array $attributes -- array of associative arrays for all levels
     * @param array $content -- array of text contents for all levels
     * @param array $namespaces -- optional array of prefix=>namespaceurl items
     *
     * @return UXMLElement -- the found or inserted node (last level)
     * @throws DomException
     */
    public function addNodePathIfNotExists($name, $attributes=null, $content=null, $namespaces=null) {
        $node = $this->selectSingleNode($name, $namespaces);
        if(!$node) {
            $nodes = explode('/', $name);
            $parent = $this;
            foreach($nodes as $i=>$nodespec) {
                $node = $parent->selectSingleNode($nodespec, $namespaces);
                $nodename = static::substring_before($nodespec, '[', true);

                if(!$node) {
                    $prefix = static::substring_before($nodename, ':');
                    if($prefix) {
                        $namespace = $namespaces[$prefix];
                        $node = $parent->addNode($nodename, static::getValue($attributes, $i), static::getValue($content, $i), $namespace);
                    } else {
                        $node = $parent->addNode($nodename, static::getValue($attributes, $i), static::getValue($content, $i));
                    }
                }

                $parent = $node;
            }
        }
        return $node;
    }

    /**
     * Creates and inserts a new text content
     *
     * @param mixed $cont -- formatted to string
     *
     * @return UXMLElement|DOMNode -- the inserted text node
     */
    public function addText($cont) {
        /** @noinspection  */
        return $this->appendChild($this->ownerDocument->createTextNode($this->ownerDocument->toString($cont)));
    }

    /** @noinspection PhpUnused */

    /**
     * Creates or replaces a text content in the node
     * (Deletes all redundant text nodes)
     *
     * @param string $cont
     *
     * @return DOMNode|UXMLElement -- the original node
     */
    public function replaceText($cont) {
        $this->nodeValue = $this->ownerDocument->toString($cont);
        return $this;
    }

    /**
     * Creates a new node with name and plain text content
     *
     * @param string $name -- the tagname of teh new node
     * @param string $cont -- the text content of the new node
     * @param string|array $namespace -- simple namespace or namespace array
     *
     * @return UXMLElement|DOMNode -- the new node created (Always DOMElement)
     */
    public function addTextNode($name, $cont, $namespace=null) {
        $prefix = static::substring_before($name, ':');
        if($prefix !='' && (is_string($namespace) && $namespace!='' || is_array($namespace) && isset($namespace[$prefix]))) {
            $ns = is_array($namespace) ? $namespace[$prefix] : $namespace;
            return $this->appendChild($this->ownerDocument->createElementNS($ns, $name, $this->ownerDocument->toString($cont)));
        }
        /** @var DOMElement $node1 */
        return @$this->appendChild($this->ownerDocument->createElement($name, $this->ownerDocument->toString($cont)));
    }

    /** @noinspection PhpUnused */

    /**
     * Creates a new node with given XML content from string
     *
     * @param string $name -- the tagname of the new node
     * @param string $cont -- the XML content in a string
     *
     * @return UXMLElement -- the new node created
     * @throws DomException
     */
    public function addHypertextNode($name, $cont) {
        $node1 = $this->ownerDocument->createElement($name);
        $node1 = $this->appendChild($node1);
        $node1->addHypertextContent($this->ownerDocument->toString($cont));
        return $node1;
    }

    /**
     * Adds XML content from a string
     * If string is not valid XML, a text node will be created
     * If string starts with <?xml, a complete XML document is assumed, and root node may be omitted
     * Otherwise the string is decoded as xml fragment, and root node cannot be omitted.
     *
     * @param string $cont - string xml content to insert. May be xml document (starting with <?xml) or xml fragment (starting with '<')
     * @param bool $strict -- throws an DomException on invalid xml string, otherwise inserts as text with (*) mark.
     * @param bool $root -- include root node of xml (not used if xml fragment is provided)
     * @param string $namespace -- insert/replace default namespace. Does not affect declared prefixed namespaces. Default is none, leaves declared
     *
     * @return DOMElement|DOMNode - (first) inserted node (may be text node)
     * @throws DomException
     */
    function addHypertextContent($cont, $strict=false, $root=false, $namespace=null) {
        if($cont instanceof DOMNode) {
            return $this->addXmlContent($cont, !$root);
        }
        if(is_null($cont)) return null;
        if(is_numeric($cont)) $cont=(string)$cont;
        if(!is_string($cont)) $cont = $this->ownerDocument->toString($cont);
        $dom2 = new DOMDocument();
        try {
            $contt = trim($cont);
            if(substr($contt,0,6)=='<?xml '  and substr($contt,-1)=='>') {
                $dom2->loadXML($contt);
                if($root) {
                    return $this->appendChild($this->ownerDocument->importNode($dom2->documentElement, true));
                }
                else {
                    $nodex = $dom2->documentElement->firstChild;
                    /** @var DOMElement $first */
                    $first = null;
                    while($nodex) {
                         $inserted = $this->appendChild($this->ownerDocument->importNode($nodex, true));
                         if(!$first) $first = $inserted;
                         $nodex = $nodex->nextSibling;
                    }
                    return $first;
                }
            }
            else if(substr($contt,0,1)=='<' and substr($contt,-1)=='>') {
                $xmlns = $namespace ? ' xmlns="'.$namespace.'"' : '';
                $r = @$dom2->loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><data$xmlns>$contt</data>");
                if(!$r) @$r = $dom2->loadXML("<?xml version=\"1.0\" encoding=\"ISO-8859-2\" ?><data$xmlns>$contt</data>");
                if(!$r) {
                    #throw new DomException('Invalid XML content '.$contt);
                    if($strict) throw new DomException('Invalid XML content');
                    return $this->addText( $cont. ' (*)');
                }
                $nodex = $dom2->documentElement->firstChild;
                /** @var DOMElement $first */
                $first = null;
                while($nodex) {
                     $inserted = $this->appendChild($this->ownerDocument->importNode($nodex, true));
                     if(!$first) $first = $inserted;
                     $nodex = $nodex->nextSibling;
                }
                return $first;
            }
            else {
                return $this->addText($cont);
            }
        }
        catch(DomException $e) {
            if($strict) throw new DomException('Invalid XML content', null, $e);
            return $this->addText($cont.' (*)');
        }
    }

    /**
     * Adds XML subtree from DOMElement or DOMDocument or xml string
     *
     * XML document or xml fragment is supported.
     *
     * @param string|DOMElement $cont - xml content to insert
     * @param boolean $omitroot - omits root node of xml document
     *
     * @return UXMLElement|DOMNode - the (first) inserted node.
     * @throws DomException on invalid XML string
     */
    function addXmlContent($cont, $omitroot=false) {
        if($cont instanceof DOMDocument) {
            if($omitroot) {
                /** @var DOMElement $first */
                $first = null;
                foreach($cont->documentElement->childNodes as $child) {
                    $inserted = $this->appendChild($this->ownerDocument->importNode($child, true));
                    if(!$first) $first = $inserted;
                }
                return $first;
            }
            return $this->appendChild($this->ownerDocument->importNode($cont->documentElement, true));
        }
        if($cont instanceof DOMElement or $cont instanceof DOMDocumentFragment) {
            if($omitroot or $cont instanceof DOMDocumentFragment) {
                /** @var DOMElement $first */
                $first = null;
                foreach($cont->childNodes as $child) {
                    $inserted = $this->appendChild($this->ownerDocument->importNode($child, true));
                    if(!$first) $first = $inserted;
                }
                return $first;
            }
            return $this->appendChild($this->ownerDocument->importNode($cont, true));
        }
        if(!is_string($cont)) $cont = $this->ownerDocument->toString($cont);
        return $this->addHypertextContent($cont, true, !$omitroot);
    }

    /**
     * Megkeresei egy node alfájában az xpath által megadott node-okat.
     *
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return DOMNodeList
     */
    function selectNodes($query, $namespaces=null) {
        $xpath = new DOMXPath($this->ownerDocument);
        if(is_array($namespaces)) foreach($namespaces as $alias=>$namespace) $xpath->registerNamespace($alias, $namespace);
        return $xpath->query($query, $this);
    }

    /**
     * Megkeresei egy node alfájában az xpath által megadott node-ot.
     *
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return UXMLElement|DOMNode -- Több node találat esetén az első node referenciájával tér vissza. Ha nincs meg, null reference (remélem)
     * @throws DomException
     */
    public function selectSingleNode($query, $namespaces=null) {
        $xpath = new DOMXPath($this->ownerDocument);
        if(is_array($namespaces)) foreach($namespaces as $alias=>$namespace) $xpath->registerNamespace($alias, $namespace);
        $result = $xpath->query($query, $this);
        if(!$result) {
            throw new DomException('Érvénytelen xpath kifejezés: '.$query, $namespaces);
        }
        return $result->item(0);
    }

    /** @noinspection PhpUnused */

    /**
     * Kiértékel egy xpath által megadott kifejezést
     *
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return mixed
     * @throws DomException
     */
    function evaluate($query, $namespaces=null) {
        $xpath = new DOMXPath($this->ownerDocument);
        if(is_array($namespaces)) foreach($namespaces as $alias=>$namespace) $xpath->registerNamespace($alias, $namespace);
        $result = $xpath->evaluate($query, $this);
        if($result===false) throw new DomException('Invalid xpath expression ' . $query);
        return $result;
    }

    /** @noinspection PhpUnused */

    /**
     * Adds one node and a subtree to the node defined in array
     * If content is not valid xml, it will be inserted as simple string content
     *
     * @param string $name -- name of the new node
     * @param string $cont -- XML content or non-xml content
     *
     * @return UXMLElement -- the original node
     */
    public function addSubtree($name, $cont) {
        $document = $this->ownerDocument;
        $node1 = $document->createElement($name);
        $this->appendChild($node1);
        $f = $document->createDocumentFragment();
        $r = $f->appendXML($cont);
        if(!$r) {
            #trace("Parsing error at uxml_add_subtree");
            $node1->addText($this->ownerDocument->toString($cont));
        }
        else {
            $node1->appendChild($f);
        }
        return $this;
    }

    /**
     * Adds text content or subnodes to a node.
     * Recursive
     *
     * @param array|string $node_list
     * - string: text content
     * - array: array of subnodes as [[nodename, attributes, subnodelist],...]
     *
     * @return UXMLElement első hozzáadott node
     * @throws DOMException
     * @throws DomException
     *
     * @deprecated use {@see addNode()}
     */
    public function addNodeList($node_list) {
        $firstnode = null;
        for($i=0; $i<count($node_list); $i++) {
            if(is_array($node_list[$i])) {
                $node_array = $node_list[$i];
                if(!isset($node_array[1])) $node_array[1]=null;
                $node1 = $this->addNode($node_array[0], $node_array[1]);
                if(isset($node_array[2])) {
                    if(is_array($node_array[2])) /** @noinspection PhpDeprecationInspection */
                        $node1->addNodeList($node_array[2]);
                    elseif(is_string($node_array[2])) $node1->addText($node_array[2]);
                    }
                }
            else $node1 = $this->addText($node_list[$i]);
            if(!$firstnode) $firstnode=$node1;
        }
        return $firstnode;
    }

    /** @noinspection PhpUnused */

    /**
     * Returns the first named subnode or creates if not exists
     *
     * @param string $nodename
     *
     * @return UXMLElement first node with this name if exists or created node if not
     */
    public function getOrCreateElement($nodename, $deep=false) {
        $document = $this->ownerDocument;
        $node = null;
        if($deep) {
            $nodelist = $this->getElementsByTagname($nodename);
            if($nodelist->length) $node = $nodelist->item(0);
        }
        else {
            foreach($this->childNodes as $childNode) {
                /** @var UXMLElement $childNode */
                if($childNode->nodeName == $nodename) { $node = $childNode; break; }
            }
        }

        if(!$node) {
            $node = $this->appendChild($document->createElement($nodename));
        }
        return $node;
    }

    /**
     * Sets attributes for bit values of the value by a given bitmap definition
     *
     * @param array $bitarray [[value, name, descr, const],...] eg. bitmap definition
     * @param int $value -- the value to represent by bit-attributes
     * @param int $field index of name column in bitarray
     * @param string $prefix -- attribute name prefix to be attached
     *
     * @return UXMLElement -- the node itself
     * @throws DomException
     */
    public function setNodeBits($bitarray, $value, $field=3, $prefix='') {
        if(!is_array($bitarray)) throw new DomException('Invalid bitarray');
        foreach($bitarray as $key=>$rb) {
            $bv = (int)$rb[0];
            $vb = ((int)$value & $bv)>0?'1':'0';
            $this->setAttribute($prefix.$rb[$field], $vb);
        }
        return $this;
    }

    /** @noinspection PhpUnused */

    /**
     * ## Sets attributes for bit values on all child nodes.
     * Skips nodes where valueattr is not found.
     *
     * @param array $bitarray [[value, name, descr, const],...] eg. bitmap definition
     * @param string $valueattr -- the attribute holding value to represent by bit-attributes
     * @param int $field index of name column in bitarray
     *
     * @return UXMLElement -- the parent node itself
     * @throws DomException
     * @see setNodeBits
     *
     */
    public function setChildNodeBits($bitarray, $valueattr, $field=3) {
        $node = $this->firstChild;
        while($node) {
            $value = $node->getAttribute($valueattr);
            if(is_numeric($value)) $node->setNodeBits($bitarray, $value, $field);
            $node = $node->nextSibling;
        }
        return $this;
    }

    /** @noinspection PhpUnused */

    /**
     * Creates nodes for all bits
     *
     * @param array $bitarray [bit=>[value, name, descr, const],...]
     * @param string $nodename
     * @param int $json
     *
     * @return UXMLElement -- the node
     * @throws DomException
     */
    public function loadBitNodes($bitarray, $nodename, $json=0) {
        if(!is_array($bitarray)) throw new DomException('Invalid bitarray');
        foreach($bitarray as $bit=>$rb) {
            $value = $rb[0];
            //$bit = strlen(decbin($value))-1;
            /** @var UXMLDoc $doc */
            $this->addNode($nodename, [
                'bit'=>$bit,
                'value'=>$value,
                'name'=>$rb[1],
                'descr'=>$rb[2],
                'const'=>$rb[3],
            ]);
            if($json) $this->setAttribute('json', json_encode($bitarray));

        }
        return $this;
    }

    /** @noinspection PhpUnused */

    /**
     * Removes all children of the node
     *
     * @return UXMLElement -- the original node
     */
    public function removeChildren() {
        $cha = $this->childNodes;
        $chc = $cha->length;
        #tracex('removeChildren: length', $chc);
        for($i=$chc-1;$i>=0;$i--) $this->removeChild($cha->item($i));
        return $this;
    }

    /**
     * @param string $alias
     * @param string $namespace
     */
    function addNamespaceDeclaration($alias, $namespace) {
        $dom = $this->ownerDocument;
        $this->appendChild($dom->createAttribute('xmlns:'.$alias))->appendChild($dom->createTextNode($namespace));
    }

    /** @noinspection PhpUnused */

    /**
     * Dumps subtree of node to XML string without the referred node itself
     *
     * @return string
     */
    function saveContent() {
        $result = '';
        $node_x = $this->firstChild;
        while($node_x) {
            $result .= $this->ownerDocument->saveXML($node_x);
            $node_x = $node_x->nextSibling;
        }
        return $result;
    }

    /** @noinspection PhpUnused */

    /**
     * # UXMLElement::createNode()
     *
     * Creates single XML node from a model or attribute array under current node (analogous to Xlet objects' createNode)
     *
     * ### Options:
     * - nodeName: the name of created node, default is 'item'
     * - attributes: the attributes to generate, default is null = all, or associative array to map attributes (alias=>attribute)
     * - contentField: name of attribute to include in node content (default none) (content field is excluded from attributes)
     * - contentValue: value of content, effective only if contentField is not given or has no value.
     * - subnodeFields: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). DOM values result nativ xml values
     *        Instead of fieldname you may specify:
     *        - callable($index, $model); callable may generate scalar or array, scalar may be an xml (DOMElement)
     *        - array:  multiple literal node values (recursive, {@see createSubnode()})
     * - extra: extra attributes array(name=>function(index, item),...)
     * - overwrite: true = overwrite/complement existing node
     *
     * Explicit declared and same-name associations are recursive.
     *
     * @param array|object $value -- node data or models
     * @param array $options
     * @param string|array|null $namespace
     *
     * @return UXMLElement -- the inserted node
     *
     * @throws DomException
     * @see Model::createNode()    for model options
     */
    function createNode($value, $options, $namespace=null) {
        return $this->createNodes([$value], $options, $namespace);
    }

    /**
     * # UXMLElement::createNodes()
     *
     * Creates multiple XML nodes from an array under current node (analogous to Xlet objects' createNodes)
     *
     * ### Options:
     * - nodeName: the name of created node, default is 'item'
     * - attributes: the attributes to generate, default is null = all, or associative array to map attributes (alias=>attribute)
     * - contentField: name of original attribute to include in node content (default none) (content field is excluded from attributes)
     * - contentValue: value of content, effective only if contentField is not given or has no value.
     * - subnodeFields: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). DOM values result nativ xml values
     *        Instead of fieldname you may specify:
     *        - callable($values, $index); callable may generate universal node data (see {@see addContent})
     *        - array:  multiple literal node values (see {@see addContent}) named alias
     * - extra: extra attributes array(name=>function(values, index),...) resulting attribute value
     *
     * Explicit declared and same-name associations are recursive.
     *
     * This version handles only string values (TODO: use external formatter)
     *
     * @param array[]|object[] $data -- array of node data arrays or models
     * @param array $options
     * @param string|array|null $namespace
     *
     * @return UXMLElement -- the first inserted node
     *
     * @throws DomException
     * @see Model::createNode()    for model options
     * TODO: check usages, behavior changed!
     */
    function createNodes($data, $options, $namespace=null) {
        if(!is_array($data)) throw new DomException('array expected');

        $nodename = static::getValue($options, 'nodeName', 'item');
        /** @var array $attributes -- finally it will be alias=>fieldname map*/
        $attributes = static::getValue($options, 'attributes');
        $contentField = static::getValue($options, 'contentField');
        $defaultContentValue = static::getValue($options, 'contentValue', null);
        $subnodeFields = static::getValue($options, 'subnodeFields', []);
        $extra = static::getValue($options, 'extra', []);
        $contentValue = $defaultContentValue;
        $defaultAttributes = isset($data[0]) && is_array($data[0]) ? array_keys($data[0]) : [];

        if($contentField!==null) {
            $ax = array_search($contentField, $defaultAttributes);
            if($ax!==false) unset($defaultAttributes[$ax]);
        }

        if($attributes===null) $attributes = $defaultAttributes;

        // Replaces integer keys to itself in attributes map.
        foreach($attributes as $n=>$f) {
            if(is_int($n)) {
                if(!is_int($f)) $attributes[$f] = $f;
                unset($attributes[$n]);
            }
        }

        foreach($subnodeFields as $snf) {
            if(is_string($snf)) unset($attributes[array_search($snf, $attributes)]);
        }

        $firstNode = null;
        foreach($data as $index=>$values) {
            /** @var array $attributevalues -- associative array of attributename=>attributevalue pairs */
            $attributevalues = array_combine(
                array_keys($attributes),
                array_map(function($key) use($values) {
                    return static::getValue($values, $key);
                }, array_values($attributes))
            );

            if($contentField!==null) {
                $contentValue = static::getValue($values, $contentField);
                if($contentValue===null) $contentValue = $defaultContentValue;
            }

            // Creating subnodes
            foreach ($subnodeFields as $key => $snf) {
                $tagname = is_int($key) ? (is_string($snf) ? $snf : 'item_' . $key) : $key;
                if (is_callable($snf)) {
                    $attributevalues[$tagname] = call_user_func($snf, $values, $index);
                }
                else if(is_string($snf)) {
                    $attributevalues[$tagname] = [static::getValue($values, $snf)];
                }
                else if (is_array($snf)) {
                    $attributevalues[$tagname] = $snf;
                }
                else {
                    $attributevalues[$tagname] = [$snf];
                }
            }

            // extra (computed) attributes (array valus generate subnodes!)
            foreach ($extra as $key => $fn) {
                $name = is_int($key) ? (is_string($fn) ? $fn : 'item_' . $key) : $key;
                if (is_callable($fn)) {
                    $attributevalues[$name] = call_user_func($fn, $values, $index);
                }
                else {
                    $attributevalues[$name] = $fn;
                }
            }

            $node = $this->addNode($nodename, $attributevalues, $contentValue, $namespace);
            if(!$firstNode) $firstNode = $node;
        }
        return $firstNode;
    }

    /**
     * Generates a subnode of the node using value array, recursively
     *
     * @param string $tagname -- nodename
     * @param string|array|null $value -- data of the subnode
     * 		  - null makes to skip subnode/attribute creation
     *        - string will be text content
     *        - associative scalar values are attributes
     *        - associative values of arrays are lists of subnodes
     *        - numbered scalar values are text nodes
     *        - numbered array values are illegal
     * @param boolean $snf -- generate hypertext content from value (string expected)
     *
     * @return DOMElement -- the inserted subnode
     * @throws DOMException
     * @throws DomException
     *
     * @deprecated use {@see addNode()}
     */
    public function createSubnode($tagname, $value, $snf=null) {
        if(!is_null($value)) {
            $subnode = $this->addNode($tagname);
            if($snf || ($value instanceof DOMNode)) {
                $subnode->addHypertextContent($value, true, true);
            }
            else {
                if(is_array($value)) {
                    foreach($value as $k=>$v) {
                        if(is_int($k)) {
                            if($v!==null && $v!='') $subnode->addText($v);
                        }
                        else if(is_array($v)) {
                            foreach($v as $sv) /** @noinspection PhpDeprecationInspection */
                                $subnode->createSubnode($k, $sv);
                        }
                        else if(is_object($v)) throw new DomException('String expected');
                        else if($v!==null) $subnode->addAttribute($k, $v);
                    }
                }
                else $subnode->addText($this->ownerDocument->toString($value));
            }
            return $subnode;
        }
        return null;
    }

    /**
     * Generates content of the node
     *
     * ### contentData:
     *
     * - string: single text content
     * - DOMElement or DOMDocument: subtree
     * - array:
     * 		- string index -> scalar: attribute of parent node
     *		- string index -> array of contentData: multiple subnodes with same (index) name, content data for each
     * 		- numeric index -> scalar: text content (DOM: subtree)
     * 		- numeric index -> array of subNode
     *
     * ### subNode:
     * 		- first numeric item is nodename or [nodename, namespace] array, the rest is content data.
     * 		- if no numeric item, or not a nodename, content will be applied to parent
     *
     * ## Example
     *
     * <parent>
     * 		<subnode id="1">Első</subnode>
     * 		<subnode id="2">Második</subnode>
     * 		<subnode id="3">
     * 			<n2>21</n2><n2>22</n2><n3>33</n3>
     * 		</subnode>
     * 		Textnode
     * </parent>
     *
     * [
     * 		'subnode' => [				// or ['subnode', 'id'=>1, 'Első'], ['subnode', 'id'=>2, ...
     * 			['id'=>1, 'Első'],
     * 			['id'=>2, 'Második'],
     * 			['id'=>3,
     *				'n2' => [21, 22],		// or ['n2', 21], ['n2', 22],
     *				'n3' => [33],  		// or ['n3', 33]
     * 			],
     * 		'Textnode',
     * ]
     *
     * @param string|mixed[]|null $contentData -- Universal content data
     * 		  - null makes to skip subnode/attribute creation
     *        - scalar will be text content
     *        - associative scalar values are attributes (boolean will not be localized)
     *        - associative values of arrays are lists of subnodes
     *        - numbered scalar values are text nodes
     *        - numbered array values are illegal
     *
     * @param string|array $nameSpace
     *
     * @return UXMLElement -- the parent node itself
     * @throws DOMException
     * @throws DomException
     */
    public function addContent($contentData, $nameSpace=null) {
        if(is_null($contentData)) return $this;
        if(is_array($contentData)) {
            foreach($contentData as $k => $v) {
                if(is_int($k)) {
                    if(is_array($v)) { // subnode
                        if(isset($v[0]) && (is_string($v[0]) || is_array($v[0]) && isset($v[0][0]) && is_string($v[0][0]))) {
                            $nodeName1 = array_shift($v);
                            if(is_array($nodeName1)) {
                                $nameSpace1 = static::getValue($nodeName1, 1);
                                $nodeName1 = $nodeName1[0];
                                $prefix1 = static::substring_before($nodeName1, ':');

                                if($prefix1) $nameSpace[$prefix1] = $nameSpace1;
                                else {
                                    if($nameSpace[0]) $nameSpace[1] = $nameSpace[0];
                                    $nameSpace[0] = $nameSpace1;
                                }
                            }
                            if($nodeName1) $this->addNode($nodeName1, $v, null, $nameSpace);
                        }
                        else $this->addContent($v, $nameSpace);
                    }
                    // numeric => scalar
                    else if($v !== null && $v != '') $this->addContent($v, $nameSpace);
                }
                // String => contentData
                else if(is_array($v)) $this->addNodes($k, $v, $nameSpace);
                // String => scalar
                else {
                    if($v !== null) $this->addAttribute($k, $v, $nameSpace);
                }
            }
        }
        else if($contentData instanceof DOMNode) {
            $this->addHypertextContent($contentData, true, true, $nameSpace);
        }
        else { // single scalar
            $this->addText($contentData);
        }
        return $this;
    }

    /**
     * Retrieves and removes the value of an array element with the given key.
     * If the key does not exist in the array, the default value will be returned.
     *
     * The key must be scalar.
     *
     * Examples
     *
     * ```php
     * // working only with array
     * $username = ArrayUtils::fetchValue($_POST, 'username');
     * ```
     *
     * @param array $array -- array to extract value from
     * @param string $key -- key name
     * @param mixed $default the default value to be returned if the specified array key does not exist
     *
     * @return mixed the value of the element if found, default value otherwise
     */
    public static function fetchValue(&$array, $key, $default = null) {
        static::assertArray($array);
        if(isset($array[$key]) || array_key_exists($key, $array)) {
            $result = $array[$key];
            unset($array[$key]);
            return $result;
        }
        return $default;
    }

    /**
     * If argument is not an array, throws exception
     *
     * @param mixed $x
     *
     * @return void
     */
    public static function assertArray($x) {
        if(!is_array($x)) throw new Exception('#1:parameter must be an array, got ' . gettype($x) . ': ' . print_r($x, true));
    }

    /**
     * Assures that $entity is an instance of $class or null
     *
     * @param object $entity
     * @param string $class
     * @param bool $null may be null
     *
     * @return void
     */
    static function assertClass($entity, $class, $null = true) {
        if($null && $entity === null) return;
        if($entity === null) throw new Exception("Parameter must not be null");
        if(!is_object($entity)) throw new Exception("Parameter must be object of $class, got " . gettype($entity));
        if(!is_a($entity, $class)) throw new Exception("Parameter must be a $class, got " . get_class($entity));
    }

    /**
     * Returns relative path of this Element from the '$reference' path
     *
     * For example, if current node is '/data/content/unit[1]', the reference node is '/data/content/form[2]',
     * the result is '../unit[1]'
     *
     * /data/content/form[2] --- /../../../data/content/unit[1]
     *
     *
     * @param UXMLElement $reference
     * @return string -- relative xpath of this node
     */
    public function relativePath($reference) {
        $path1 = explode('/', $this->getNodePath());
        $path2 = explode('/', $reference->getNodePath());
        $b = count($path2);
        while(count($path1) && count($path2) && $path1[0]==$path2[0]) {
            array_shift($path1);
            array_shift($path2);
            $b--;
        }
        return implode('/', array_merge(array_fill(0, $b, '..'), $path1));
    }

    /**
     * Returns the value of a property.
     *
     * @param string $name the property name
     *
     * @return mixed the property value
     * @throws InternalException if the property is not defined or the property is write-only.
     * @see __set()
     * @noinspection DuplicatedCode
     */
    public function __get($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (method_exists($this, 'set' . $name)) {
            throw new InternalException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new InternalException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    public function getXml() {
        return $this->ownerDocument->saveXML($this);
    }
}

/**
 * Class UXMLNodeList
 *
 * @method UXMLElement item() item(int $num)
 */
class UXMLNodeList extends DOMNodeList {

}
