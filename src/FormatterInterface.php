<?php


namespace uhi67\uxml;


use DateTime;
use IntlDateFormatter;

/**
 * # Interface FormatterInterface
 *
 * FormatterInterface declares a formatter class. The main purpose of the formatter class is to convert various data
 * to a localized human-readable string which can be inserted into xml values.
 *
 * Formatter may have a default locale or use the specified locale given in method arguments.
 *
 * Other methods:
 * - formatDateTime() - to localize date of any formats
 *
 * @package uhi67\uxml
 */
interface FormatterInterface {
	/**
	 * @param mixed $value
	 * @param string|null $locale
	 *
	 * @return string
	 */
	public function toString($value, $locale=null);

	/**
	 * @param DateTime|int|string $value
	 * @param int|null $dateType -- default is short
	 * @param int|null $timeType -- default is none
	 * @param string|null $locale
	 *
	 * @return string
	 */
	public function formatTimeValue($value, $dateType=IntlDateFormatter::SHORT, $timeType=IntlDateFormatter::NONE, $locale=null);

	/**
	 * @param string $locale
	 * @return string
	 */
	public function setLocale($locale);
	/**
	 * @return string
	 */
	public function getLocale();

	public function format($value, $format);

}
