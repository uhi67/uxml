<?php
namespace uhi67\uxml;

use DOMDocument;
use DOMElement;
use DOMException;
use XSLTProcessor;

/**
 * # Class UXMLDoc
 *
 * Extended DOM functions
 *
 * @property UXMLElement $documentElement
 * @method UXMLElement createElement() createElement(string $name, string $value=null)
 * @method UXMLElement createElementNS() createElementNS(string $namespace, string $qualifiedName, string $value=null)
 */
class UXMLDoc extends DOMDocument {
	/** @var string $xslpath -- for XSL processing */
	public $xslpath;
	/** @var string $locale -- for attribute conversion */
	public $locale = null;
	/** @var FormatterInterface $formatter */
	public $formatter = null;

	/**
	 * @param string $enc
	 * @param string $root -- name of the root node, skips if empty
	 * @param string $xslpath -- location of xsl files for process() method (no default)
	 * @param string $locale -- no default
	 * @param FormatterInterface $formatter -- null to use default formatter, false to disable formatter
	 */
	function __construct($enc='UTF-8', $root='data', $xslpath=null, $locale=null, $formatter=null) {
		parent::__construct('1.0', $enc);
		$this->formatOutput = true;
		$this->preserveWhiteSpace = false;
		$this->encoding = $enc;
		$this->substituteEntities = false;
		$this->resolveExternals = false;

		$this->registerNodeClass('DOMElement', UXMLElement::class);

		if($root) $this->appendChild($this->createElement($root));
		$this->xslpath = $xslpath;
		$this->locale = $locale;
		$this->formatter = $formatter ? $formatter : new Formatter(['locale'=>$locale]);
	}

	/** @noinspection PhpUnused */

	/**
	 * Formats XML fragment string to standard format
	 *
	 * @param string $xml
	 *
	 * @return string
	 */
	public static function formatFragment($xml) {
		$doc = UXMLDoc::createFragment($xml, 'UTF-8', true);
		$doc->preserveWhiteSpace = false;
		$doc->formatOutput = true;
		return $doc->saveXML($doc->documentElement->firstChild);
	}

	/** @noinspection PhpUnused */

	/**
	 * Processes an DOM document using an XSLT loaded from given file
	 *
	 * @param DOMDocument $xmlsource
	 * @param string $xslfile -- full path
	 * @param array $params -- global accessable parameters in XSLT processing
	 * @param bool $xml -- true = XML string output, false = DOMDocument (default)
	 *
	 * @return DOMDocument|string
	 * @throws DomException
	 */
	static function processXml($xmlsource, $xslfile, $params=NULL, $xml=false) {
		if(!class_exists('XSLTProcessor')) throw new DomException('XSL module is missing');
		if(is_null($xmlsource)) throw new DomException('XML document is null');
		$xsldoc = new XSLTProcessor();
		$xmldoc = new DomDocument();
		if(!file_exists($xslfile)) echo "Nincs meg a fájl: $xslfile";
		$xmldoc->load($xslfile);
		$xsldoc->importStylesheet($xmldoc);
		if(is_array($params)) /** @noinspection PhpParamsInspection -- error in definition, two-params version does exist */
			$xsldoc->setParameter('', $params);
		if($xml) {
			$result = $xsldoc->transformToXml($xmlsource);
		}
		else {
			$result = $xsldoc->transformToDoc($xmlsource);
			$result->formatOutput = true;
		}
		return $result;
	}

	/**
	 * Processes the document into a new DOMDocument or string using given xsl
	 *
	 * @param string $xslfile -- name of xsl file. Default is located in xslpath
	 * @param array $params -- global parameters accessible in xsl variables
	 * @param bool $xml -- transform into XML string (default: to DOMDocument)
	 *
	 * @return DomDocument|string -- the resulting document
	 * @throws DomException
	 */
	function process($xslfile, $params=NULL, $xml=false) {
		try {
			#$result = new DomDocument(); // Empty for exceptions
			if(!class_exists('XSLTProcessor')) throw new DomException('XSL module is missing');
			$xsldoc = new XSLTProcessor();
			$xmldoc = new DomDocument();
			if(!file_exists($xslfile)) throw new DomException('Nincs meg a fájl ' . $xslfile);
			$xmldoc->load($xslfile);
			if(!$xmldoc) {
				$ee = error_get_last();
				throw new DomException("Error loading `$xslfile`: ".print_r($ee, true));
			}
			if($this->xslpath) {
				$xmldoc->documentURI = $this->xslpath . '/' . pathinfo($xslfile, PATHINFO_FILENAME);
			}
			else {
				$xmldoc->documentURI = $xslfile;
			}
			if(!@$xsldoc->importStylesheet($xmldoc)) {
				$ee = error_get_last();
				$message = UXMLElement::getValue($ee, 'message');
				// Filter `xsl:include : unable to load file:/D:/data/www/rr/xsl/samlres.xsl`
				if(preg_match('~unable to load file:(.+)$~', $message, $mm)) {
					$filename = $mm[1];
					if(function_exists('error_clear_last')) {
						/** @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection */
						error_clear_last();
					}
					if(!file_exists($filename)) throw new DomException("Include file `$filename` not exists in `$xslfile`: ".$ee['message']);
					$include = UXMLDoc::load($filename);
					$ee = error_get_last();
					if(!$include) {
						throw new DomException("Error loading include `$filename` into `$xslfile`: ".$ee['message']);
					}
				}
				throw new DomException("Error interpreting `$xslfile`: ".$message);
			}

			if(is_array($params)) /** @noinspection PhpParamsInspection */
				$xsldoc->setParameter('', $params);
			if($xml) {
				$result = $xsldoc->transformToXml($this);
				if(!$result) throw new DomException("XSL transformation failed in '$xslfile'");
			}
			else {
				$result = $xsldoc->transformToDoc($this);
				if(!$result) throw new DomException("Error processing stylesheet $xslfile");
				$result->formatOutput = true;
			}
		}
		catch(DomException $e) {
			$ee = error_get_last();
			/** @noinspection PhpUnhandledExceptionInspection */
			throw new DomException('XSL feldolgozási hiba: '. print_r($ee, true), 0, $e);
		}
		return $result;
	}

	/** @noinspection PhpMethodNamingConventionInspection */
	/** @noinspection PhpUnused */

	/**
	 * @param string $xslfile
	 *
	 * @return DomDocument|string|null
	 * @throws DomException
	 */
	function process_xml($xslfile) {
		$result = $this->process($xslfile, null, true);
		if($result) return $result; //@$result->saveXML();
		else return null;
	}

	/** @noinspection PhpMethodNamingConventionInspection */
	/** @noinspection PhpUnused */

	/**
	 * @param string $xslfile
	 *
	 * @return string
	 * @throws DomException
	 */
	function process_html($xslfile) {
		$result = $this->process($xslfile);
		return $result->saveHTML();
	}

	/** @noinspection PhpUnused */

	/**
	 * Clones a node replacing namespace to specified (or remove)
	 *
	 * This function is based on a [comment to the PHP documentation](http://www.php.net/manual/de/domnode.clonenode.php#90559)
	 *
	 * @param DOMElement $node
	 * @param string $namespace -- null to remove namespace, false to keep original namespace
	 *
	 * @return DOMElement
	 */
	function cloneNodeNS($node, $namespace=null) {
		$unprefixedName = preg_replace('/.*:/', '', $node->nodeName);
		if($namespace == false) {
			// Keep original namespace
			$nd = $this->createElementNS($node->namespaceURI, $node->nodeName);
		}
		elseif($namespace) {
			$nd = $this->createElementNS($namespace, $unprefixedName);
		}
		else {
			$nd = $this->createElement($unprefixedName);
		}

		foreach ($node->attributes as $value)
			$nd->setAttribute($value->nodeName, $value->value);

		if (!$node->childNodes)
			return $nd;

		foreach($node->childNodes as $child) {
			if($child->nodeName == "#text")
				$nd->appendChild($this->createTextNode($child->nodeValue));
			else
				$nd->appendChild($this->cloneNodeNS($child, $namespace));
		}
		return $nd;
	}

	/**
	 * Creates a new document from xml fragment, using a new <root> node.
	 *
	 * Replaces uxml_open_fragment()
	 *
	 * @param string $xmlstring
	 * @param string $enc
	 * @param bool $strict -- if false (default), returns text node if xmlstring is invalid xml
	 *
	 * @return UXMLDoc
	 */
	static function createFragment($xmlstring, $enc='UTF-8', $strict=false) {
		$dom = new UXMLDoc($enc, 'root');
		$f = $dom->createDocumentFragment();
		$r = $f->appendXML($xmlstring);
		if($r) $dom->documentElement->appendChild($f);
		else if($strict) return null; else $dom->documentElement->addText($xmlstring);
		return $dom;
	}

	/**
	 * Converts value to human readable string using configured formatter
	 *
	 * @param mixed $value
	 * @return string
	 */
	public function toString($value) {
		if(is_string($value)) return $value;
		if($this->formatter) return $this->formatter->toString($value);
		if(is_numeric($value)) return (string)$value;
		return '('.gettype($value).')';
	}
}
