<?php

namespace uhi67\uxml;

use DateTime;
use Exception;
use IntlDateFormatter;

/**
 * # Class Formatter
 *
 * Basic formatter for following data types:
 * - bool
 * - int
 * - float
 * - array
 * - DateTime
 * - object with toString()
 *
 * locale is used only for date formatting.
 *
 * @package uhi67\uxml
 */
class Formatter implements FormatterInterface {
    /** @var string $locale -- 'la-LO' or 'la' format */
    public $locale;

    public function __construct($config) {
        if(isset($config['locale'])) $this->locale = $config['locale'];
    }

    /** @var array $dateformats: [preg, dateformat] */
    static public $dateformats = [
        ['/^\d{4}\.\d{2}\.\d{2}$/', '!Y.m.d'],
        ['/^\d{4}\.\d{2}\.\d{2}\.$/', '!Y.m.d.'],
        ['/^\d{4}\.\d{2}\.\d{2}\s\d{1,2}:\d{1,2}$/', '!Y.m.d H:i'],
        ['/^\d{4}\.\d{2}\.\d{2}\.\s\d{1,2}:\d{1,2}$/', '!Y.m.d. H:i'],
        ['/^\d{4}\.\d{2}\.\d{2}\s+\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y.m.d H:i:s'],
        ['/^\d{4}\.\d{2}\.\d{2}\.\s+\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y.m.d. H:i:s'],

        ['/^\d{4}\.\s\d{2}\.\s\d{2}$/', '!Y. m. d'],
        ['/^\d{4}\.\s\d{2}\.\s\d{2}\.$/', '!Y. m. d.'],
        ['/^\d{4}\.\s\d{2}\.\s\d{2}\.\s\d{1,2}:\d{1,2}$/', '!Y. m. d. H:i'],
        ['/^\d{4}\.\s\d{2}\.\s\d{2}\.\s\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y. m. d. H:i:s'],
    ];

    /**
     * Creates a human compact visible form of convertable objects
     *
     * @param mixed $value
     * @param string|null $locale -- null = default local; false = no internationalization
     *
     * @return string
     */
    public function toString($value, $locale=null) {
            if(is_null($value)) return '';
            if(is_bool($value)) return $value ? 'true': 'false';
            if(is_int($value)) return (string)$value;
            if(is_float($value)) return (string)$value;
            if(is_string($value)) return $value;
            if(is_array($value)) {
                if(static::isAssociative($value, false)) {
                    $result = '';
                    foreach($value as $key => $item) {
                        if($result != '') $result .= ', ';
                        $result .= $key . ':' . self::toString($item, $locale);
                    }
                    return '[' . $result . ']';
                }
                else {
                    $result = '';
                    foreach($value as $item) {
                        if($result != '') $result .= ', ';
                        $result .= self::toString($item, $locale);
                    }
                    return '[' . $result . ']';
                }
            }
            if($value instanceof DateTime) {
                if($value->format('His')=='00000') {
                    // Date only
                    if(!$locale) return $value->format('Y-m-d');
                    else return static::formatTimeValue($value, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, $locale);
                }
                if(!$locale) return $value->format(DateTime::ATOM);
                else return static::formatTimeValue($value, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT, $locale);
            }
            if(is_callable([$value , 'toString'])) return $value->toString();
            if(is_object($value)) return get_class($value);
            return '('.gettype($value).')';
    }

    public function setLocale($locale) {
        $this->locale = $locale;
    }

    public function getLocale() {
        return $this->locale;
    }

    /**
     * Check the given array if it is an associative array.
     *
     * If `$strict` is true, the array is associative if all its keys are strings.
     * If `$strict` is false, the array is associative if at least one of its keys is a string.
     *
     * An empty array will be considered associative only in strict mode.
     *
     *    - `isAssociative(array, false)` means the array has associative elements,
     *    - `!isAssociative(array, true)` means the array has non-associative elements.
     *
     * @param array $array the array being checked
     * @param bool $strict the array keys must be all strings and not empty to be treated as associative.
     * @return bool the array is associative
     */
    public static function isAssociative($array, $strict=true) {
        if (!is_array($array)) return false;
        foreach ($array as $key => $value) {
            if (!is_string($key) && $strict) return false;
            if (is_string($key) && !$strict) return true;
        }
        return $strict;
    }

    /**
     * Formats any datetime-like data using configured localization
     *
     * @param DateTime|string|int|float $value
     * @param int $datetype -- date format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'. Default is SHORT.
     * @param int $timetype -- time format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'. Default is NONE.
     * @param string $locale
     *
     * @return string -- the formatted date, empty string if $value is null or invalid date
     */
    public function formatTimeValue($value, $datetype=IntlDateFormatter::SHORT, $timetype=IntlDateFormatter::NONE, $locale=null) {
        $value = static::createDateTime($value);
        if($value===null) return '';
        return static::formatDateTime($value, $datetype, $timetype, $locale);
    }

    /**
     * formats a DateTime value using given locale
     *
     * @param DateTime $datetime
     * @param int $datetype -- date format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'
     * @param int $timetype -- time format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'
     * @param string $locale -- locale in ll-cc format (ISO 639-1 && ISO 3166-1), null to use default
     * @return string
     */
    public function formatDateTime($datetime, $datetype, $timetype, $locale=null) {
        if(!$locale) $locale = $this->locale;
        if(!$locale) $locale="en-GB";
        $pattern = null;
        if(substr($locale, 0,2)=='hu') {
            if($datetype == IntlDateFormatter::SHORT && $timetype == IntlDateFormatter::SHORT)
                $pattern = 'yyyy.MM.dd. H:mm';
            if($datetype == IntlDateFormatter::SHORT && $timetype == IntlDateFormatter::NONE)
                $pattern = 'yyyy.MM.dd.';
        }
        $dateFormatter = new IntlDateFormatter($locale, $datetype, $timetype, null, null, $pattern);
        return $dateFormatter->format($datetime);
    }

    /**
     * Converts any data type to DateTime
     *
     * @param DateTime|string|int $value
     *
     * @return DateTime|null -- null on invalid value
     */
    public static function createDateTime($value) {
        if(is_object($value) && $value instanceof DateTime) return $value;
        if(is_int($value)) return DateTime::createFromFormat( 'U', $value);
        if(!is_string($value)) return null;
        foreach(self::$dateformats as $format) {
            if(preg_match($format[0], $value)) return DateTime::createFromFormat($format[1], $value);
        }
        try {
            return new DateTime($value);
        }
        catch(Exception $e) {
            return null;
        }
    }

    /**
     * General formatting service
     *
     * Implements date and time formatting and basic string conversion
     *
     * @param $value
     * @param $format
     *
     * @return string
     */
    public function format($value, $format) {
        if(!$format) $format='string';
        switch($format) {
            case 'string':
                return $this->toString($value);
            case 'int':
                if($value instanceof DateTime) return (string)$value->getTimestamp();
                return $this->toString($value);
            case 'date':
                return $this->formatTimeValue($value, IntlDateFormatter::SHORT);
            case 'time':
                return $this->formatTimeValue($value, IntlDateFormatter::NONE, IntlDateFormatter::SHORT);
            case 'date-time':
                return $this->formatTimeValue($value, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
        }
        return null;
    }
}
